calendar (2.04-3) unstable; urgency=medium

  * Fix call to date to make the build reproducible (Closes: #932302)

 -- Stéphane Glondu <glondu@debian.org>  Wed, 17 Jul 2019 17:24:45 +0200

calendar (2.04-2) unstable; urgency=medium

  * Update Vcs-*
  * Bump Standards-Version to 4.4.0
  * Bump debhelper compat level to 12
  * Set Multi-Arch: foreign for libcalendar-ocaml-doc

 -- Stéphane Glondu <glondu@debian.org>  Tue, 16 Jul 2019 10:26:37 +0200

calendar (2.04-1) unstable; urgency=medium

  * New upstream release
  * Bump debhelper compat level to 9
  * debian/rules: switch to dh style
  * Bump Standards-Version to 3.9.6
  * Use last changelog entry for build date (for reproducibility)

 -- Stéphane Glondu <glondu@debian.org>  Tue, 19 May 2015 15:59:02 +0200

calendar (2.03.2-3) unstable; urgency=low

  * Upload to unstable
  * Update Vcs-*

 -- Stéphane Glondu <glondu@debian.org>  Tue, 03 Dec 2013 08:01:51 +0100

calendar (2.03.2-2) experimental; urgency=low

  * Compile with OCaml >= 4.01

 -- Stéphane Glondu <glondu@debian.org>  Mon, 11 Nov 2013 07:55:12 +0100

calendar (2.03.2-1) unstable; urgency=low

  * New upstream release
  * New debian/watch file by Bart Martens
  * Use format version 1.0 in debian/copyright
  * Bump Standards-Version to 3.9.4

 -- Stéphane Glondu <glondu@debian.org>  Wed, 08 May 2013 14:15:54 +0200

calendar (2.03-1) unstable; urgency=low

  [ Stefano Zacchiroli ]
  * debian/rules
    - switch {debhelper,dpatch}.mk: fix debian/*.log cleanup
  * debian/control: fix typo in long description (Closes: #557494)

  [ Stéphane Glondu ]
  * Install plugin only on natdynlink architectures
  * debian/control:
    - add myself to Uploaders, remove Stefano
    - replace Conflicts by Breaks
    - bump Standards-Version to 3.9.0

  [ Mehdi Dogguy ]
  * New upstream release.
    - Remove cmxs.dpatch (integrated upstream).
    - Remove meta.dpatch (integrated upstream).
  * Convert to 3.0 quilt format
    - Convert remaining patches to quilt format
    - Remove dpatch from b-deps
  * Remove hard-coded runtime dependencies
  * Bump dh-ocaml version to 0.9.5 in Build-Depends.
  * Update copyright file
  * Fix watch file

 -- Mehdi Dogguy <mehdi@debian.org>  Fri, 15 Apr 2011 23:55:04 +0200

calendar (2.01.1-6) unstable; urgency=low

  * debian/control
    - fix Mehdi's mail address, now that he is a DD
    - bump Standards-Version to 3.8.3 (no changes needed)
    - remove DM-Upload-Allowed (no longer needed)
  * switch to dh_ocaml for dependency calculation, add needed substvar and
    bump dh-ocaml build-dep accordingly. Keep hand-written dependency for
    the interim
  * Stop using ocamldoc-api-ref-config: it is gone (closes: #549773).
    While dh_ocamldoc lacks support for that, add a hand-written
    debian/libcalendar-ocaml-doc.doc-base .

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 06 Oct 2009 14:22:26 +0200

calendar (2.01.1-5) unstable; urgency=low

  * Update watch file
  * Fix FTBFS on non-native archs by not installing cmx files.

 -- Mehdi Dogguy <dogguy@pps.jussieu.fr>  Mon, 20 Apr 2009 10:17:07 +0200

calendar (2.01.1-4) unstable; urgency=low

  * debian/rules: installing upstream's documentation.
  * Move to new ocaml section.
  * Add cmxs.dpatch to build and install cmxs file.
  * Add two binary packages:
    - libcalendar-ocaml-doc: contains the API documentation, to not be
      forced to install libcalendar-ocaml-doc when you install
      libcalendar-ocaml-dev.
    - libcalendar-ocaml: contains the shared runtime libraries.
  * libcalendar-ocaml-dev depends on libcalendar-ocaml and suggests
    libcalendar-ocaml-doc.
  * Bump standards version to 3.8.1, no changes needed.
  * Restore upstream files after building.

 -- Mehdi Dogguy <dogguy@pps.jussieu.fr>  Sat, 21 Mar 2009 00:33:17 +0100

calendar (2.01.1-3) unstable; urgency=low

  [ Mehdi Dogguy ]
  * Add meta.dpatch to allow any executable to be linked using ocamlfind.
    (Closes: #520287)
  * Modify install_destdir.dpatch: not install cmi of then inner modules
    nor calendarLib.o
  * Add myself to uploaders
  * Add DMUA header (with Zack's blessing)

  [ Stefano Zacchiroli ]
  * debian/control: point Homepage field to the new forge project page

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 19 Mar 2009 15:53:32 +0100

calendar (2.01.1-2) unstable; urgency=low

  * debian/patches/*
    - install_destdir.dpatch: use $(wilcard...) to expand *.cmxa only if
      they are actually there. Fix FTBFS on byte archs. (Closes: #517702)

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 02 Mar 2009 23:08:38 +0100

calendar (2.01.1-1) unstable; urgency=low

  * New Upstream Version
  * Uploading to unstable
  * debian/rules: use ocaml.mk as a CDBS "rules" file
  * debian/control: bump versioned build-deps to induce dep-waits

 -- Stefano Zacchiroli <zack@debian.org>  Sat, 28 Feb 2009 22:05:15 +0100

calendar (2.01-1) experimental; urgency=low

  [ Stefano Zacchiroli ]
  * make Vcs-* fields point to the new git repository
  * new upstream version
    - refresh patch install_destdir
  * debian/dirs.in: removed, dir creation is now delegated to Makefile
  * debian/control:
    - bump Standards-Version to 3.8.0 (no changes needed)
    - debhelper compatibility level: 7 (also affects debian/compat)
    - add missing ${misc:Depends} to dependencies (thanks Lintian)
  * rebuild against OCaml 3.11
    - stricten build-deps, for experimental build
    - add build-dep on dh-ocaml (which now ships the CDBS class)
    - remove no longer needed version specific deps
  * debian/svn-deblayout: removed (we use Git now)

 -- Stefano Zacchiroli <zack@debian.org>  Wed, 18 Feb 2009 18:46:02 +0100

calendar (2.0.2-1.1) experimental; urgency=low

  * Rebuild with ocaml 3.10.2, NMU with maintainer's blessing.

 -- Ralf Treinen <treinen@debian.org>  Sat, 22 Mar 2008 21:19:52 +0100

calendar (2.0.2-1) unstable; urgency=low

  * new upstream release
  * fix vcs-svn field to point just above the debian/ dir
  * debian/patches/install_destdir.dpatch: refresh patch for new upstream

 -- Stefano Zacchiroli <zack@debian.org>  Fri, 21 Mar 2008 14:52:33 +0100

calendar (1.10-3) unstable; urgency=low

  * add Homepage field to debian/control
  * update standards-version, no changes needed
  * setting me as an uploader and d-o-m as package maintainer

 -- Stefano Zacchiroli <zack@debian.org>  Fri, 28 Dec 2007 19:21:08 +0100

calendar (1.10-2) unstable; urgency=low

  * bump debhelper build dep and compatibility level to 5
  * debian/control
    - bump ocaml-nox build dep to >= 3.10.0-8, to ensure the correct CDBS
      class is used
  * debian/rules
    - use the CDBS way of generating ocamldoc documentation, instead of the
      manual one (as a consequence also remove the "doc/" entry from
      debian/docs)
  * debian/doc-base
    - remove entry, now it is automatic generated during build

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 03 Sep 2007 22:10:19 +0200

calendar (1.10-1) experimental; urgency=low

  * new upstream release
  * rebuild with OCaml 3.10
  * debian/patches/
    - renamed 17_Makefile to install_destdir (more meaningful name)

 -- Stefano Zacchiroli <zack@debian.org>  Fri, 13 Jul 2007 10:49:53 +0200

calendar (1.09.3-8) unstable; urgency=low

  * debian/control
    - changed build dependency from ocaml to ocaml-nox (no need of X for
      calendar!)
    - bumped build dependency on ocaml-nox to >= 3.09.2-7 since we now use the
      ocaml.mk cdbs class shipped from that version on
  * debian/watch
    - switched to uscan format version 3

 -- Stefano Zacchiroli <zack@debian.org>  Sat,  4 Nov 2006 09:33:14 +0100

calendar (1.09.3-7) unstable; urgency=low

  * debian/rules
    - removed no longer needed workaround for cdbs + dpatch
    - avoid to create debian/control from debian/control.in on ocamlinit
    - removed from the source package files which are generated at build time
      from the corresponding .in files
  * debian/control.in
    - file removed, no longer needed

 -- Stefano Zacchiroli <zack@debian.org>  Wed,  6 Sep 2006 09:51:53 +0200

calendar (1.09.3-6) unstable; urgency=low

  * Upload to unstable.

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 16 May 2006 20:09:40 +0000

calendar (1.09.3-5) experimental; urgency=low

  * Rebuilt against OCaml 3.09.2, bumped deps accordingly.
  * Bumped Standards-Version to 3.7.2 (no changes needed).

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 11 May 2006 22:01:57 +0000

calendar (1.09.3-4) experimental; urgency=low

  * debian Rebuilt against OCaml 3.09.2, bumped deps accordingly.

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 11 May 2006 21:54:03 +0000

calendar (1.09.3-3) unstable; urgency=low

  * Rebuilt against OCaml 3.09.1, bumped deps accordingly.

 -- Stefano Zacchiroli <zack@debian.org>  Sat,  7 Jan 2006 11:47:14 +0100

calendar (1.09.3-2) unstable; urgency=low

  * Rebuilt with ocaml 3.09
  * debian/*
    - removed hard coding of ocaml abi version
    - use cdbs
  * debian/control
    - bumped standards version

 -- Stefano Zacchiroli <zack@debian.org>  Tue,  8 Nov 2005 22:54:00 +0100

calendar (1.09.3-1) unstable; urgency=low

  * New upstream release
  * Rebuilt against ocaml 3.08.3
  * debian/{control,rules,patches/}
    - uses dpatch
  * debian/rules
    - no longer manually invoke ocamldoc, upstream tarball comes with prebuilt
      ocamldoc documentation
  * debian/docs
    - install new version of the FAQ and TODO file
    - don't install .ps.gz documentation, no longer shipped upstream

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 24 Mar 2005 09:37:49 +0100

calendar (1.09.1-2) unstable; urgency=low

  * Rebuilt against ocaml 3.08.2

 -- Stefano Zacchiroli <zack@debian.org>  Thu,  9 Dec 2004 16:02:22 +0100

calendar (1.09.1-1) unstable; urgency=low

  * New upstream release

 -- Stefano Zacchiroli <zack@debian.org>  Fri, 19 Nov 2004 10:51:06 +0100

calendar (1.08-1) unstable; urgency=low

  * New upstream release
  * rebuilt with ocaml 3.08
  * debian/control
    - bumped ocaml deps to 3.08
    - bumped standards-version to 3.6.1.1
    - changed ocaml deps to ocaml-nox

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 26 Jul 2004 18:14:10 +0200

calendar (1.06-3) unstable; urgency=low

  * Makefile.in
    - fix FTBFS on architectures that have no native code compiler
      (Closes: Bug#238217)

 -- Stefano Zacchiroli <zack@debian.org>  Wed, 17 Mar 2004 13:35:54 +0100

calendar (1.06-2) unstable; urgency=low

  * Ship also ocamldoc HTML documentation (Closes: Bug#238174)

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 15 Mar 2004 21:02:53 +0100

calendar (1.06-1) unstable; urgency=low

  * Initial Release.

 -- Stefano Zacchiroli <zack@debian.org>  Wed, 25 Feb 2004 12:44:43 +0100
